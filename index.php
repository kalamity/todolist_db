
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
<div class="mainbox">
    <div class="inputbox">
        <h2 style="margin:5px">My To Do List</h2>
        <!--Insert to Database -->
        <form  action="addtodobd.php" method="POST">
            <input class="inputBoxText" type="text" name="text" placeholder="Quelque chose à faire ?">
            <input type="submit" class="addBtn" value="Ajouter">
        </form>
    </div>
    <div>
    <!--Connection to the Sql Database -->
        <?php
        include('config.php');
        $bdd = getConnection();
        $response=$bdd->query('SELECT*FROM todo');
        ?>
        <ul>
        <?php
            while ($donnees = $response->fetch()){?>
            <li>
        <!--Update Database -->
            <form action="updatetodobd.php" method='POST'>
            <span>
                <input class="strikethrough"type="checkbox" name="done" value="1" <?php if ($donnees ['done']==1) {echo "checked";} ?>>
                <input class="todoText" type="text" name="afaire" value="<?php echo $donnees['afaire']; ?>"/>
            </span>
                <input type="hidden" name="id" value="<?php echo $donnees['id']; ?>"/>
                <button class="saveBtn"type="submit" name="save">
                    <i class="material-icons">save</i>
                </button>
            </span>
            </form>
    <!--Delete Database -->
            <form action="deletetodobd.php" method="POST" class="form-ajout">
            <span>
                <input type="hidden" name="id" value="<?php echo $donnees['id']; ?>"/>
                <button class="delBtn"type="submit" name="delete">
                    <i class="material-icons">delete</i>
                </button>
            </span>
            </form>
            </li>
        <?php
            }
        ?>
        </ul>
</div>
<body>
</html>


