<?php
// DB is a class created for the CRUD function

include_once('config.php');//Connection to the database
class DB {
//Create
    static function add($text){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO todo (afaire) VALUES (:afaire)');
        $req->execute(array('afaire' => $text));
    }
//Delete
    static function delete($id){
        $bdd = getConnection();
        $req = $bdd->prepare('DELETE FROM todo WHERE id=:id');
        $req->execute(['id'=>$id]);

    }
//Update
    static function update($id,$text, $done){
        $bdd = getConnection();
        $done = (int) $done;
        $req = $bdd->prepare('UPDATE todo SET afaire=:afaire, done=:done WHERE id=:id');
        $req->execute(array('afaire' => $text,'done'=>  $done,'id' => $id));
    }

}
?>